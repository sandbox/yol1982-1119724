<?php


function priority_views_data(){

    $data = array();

    $data['priority']['table']['group'] = t('Priority');
    $data['priority']['table']['base'] = array(
        'field' => 'nid',
        'title' => t('Priority table'),
        'help' => t("Priority table contains your defined priority"),
        'weight' => -10,
    );

    $data['priority']['table']['join'] = array(
        // Index this array by the table name to which this table refers.
        // 'left_field' is the primary key in the referenced table.
        // 'field' is the foreign key in this table.
        'node' => array(
            'left_field' => 'nid',
            'field' => 'nid',
        ),
    );


    // Node ID field.
    $data['priority']['nid'] = array(
        'title' => t('Node id'),
        'help' => t('The node id that uses this priority'),
        // Because this is a foreign key to the {node} table. This allows us to
        // have, when the view is configured with this relationship, all the fields
        // for the related node available.
        'relationship' => array(
        'base' => 'node',
        'field' => 'nid',
        'handler' => 'views_handler_relationship',
        'label' => t('Priority node'),
        ),
    );


    // Example numeric text field.
    $data['priority']['priority'] = array(
        'title' => t('Priority number'),
        'help' => t('The priority number. Lower is higher.'),
        'field' => array(
            'handler' => 'views_handler_field_numeric',
            'click sortable' => TRUE,
        ),
        'filter' => array(
            'handler' => 'views_handler_filter_numeric',
        ),
        'sort' => array(
            'handler' => 'views_handler_sort',
        ),
    );


    $data['priority']['content_type'] = array(
        'title' => t('Content type'),
        'help' => t('Content type of node to be prioritised'),
        'field' => array(
            'handler' => 'views_handler_field',
            'click sortable' => FALSE
        )
    );



    return $data;

}