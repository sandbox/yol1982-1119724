<?php
// $Id: priority_widget.tpl.php $
/**
 * @file priority_widget.tpl.php
 */
?>
<div class="priority-widget" style="z-index:<?php print $cid?>">
    <div style="font-size:9px"><?php print $priority_up_label?></div>
    <span id="priority_up_<?php print $cid; ?>" class="priority_change <?php print $priority_up; ?>" title="<?php print $priority_up_label ?>" uri="<?php print $priority_up_uri; ?>"></span>
    <span id="priority_down_<?php print $cid; ?>" class="priority_change <?php print $priority_down; ?>" title="<?php print $priority_down_label ?>" uri="<?php print $priority_down_uri; ?>"></span>
    <div style="font-size:9px"><?php print $priority_down_label?></div>
</div>