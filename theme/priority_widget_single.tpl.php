<?php
// $Id: priority_widget_single.tpl.php $
/**
 * @file priority_widget_single.tpl.php
 */
?>
<div class="priority-widget" style="z-index:<?php print $cid?>">
    <div class="single_priority_label"><?php print $priority_label?></div>
    <div class="single_priority" uri="<?php print $priority_up_uri; ?>"><?php print $priority?></div>
</div>