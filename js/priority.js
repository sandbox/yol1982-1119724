Drupal.behaviors.priority = function (context) {
    $('.priority_change').live('click', priority_change);
    $('.single_priority').live('click', priority_select);
};

priority_select = function(e) {
    $target = $(e.target)
    priority = $target.text();
    uri = $target.attr('uri');
    html = $target.html();
    
    $input = $('<input type="text" class="priority_singe_input" value="' + priority + '"/>');
    $(e.target).html($input);

    $input.keypress(function(e){
        if (e.which == '13') {
            new_priority = $(e.target).val()
            if (IsNumeric(new_priority)) {
                uri = uri.replace(/priority_holder/, new_priority);
                $target.html(html);
                priority_single_change(uri, $target);
            }
            else{
                $target.html(html);
            }

        }

    });
    $input.trigger("focus");

    
}
function IsNumeric(input)
{
   return (input - 0) == input && input.length > 0;
}


priority_single_change = function(uri, $target) {
    $.ajax({
        type: 'GET',
        url: uri,
        success: function (data) {
            $target.text(data);
        }
    });

}

priority_change = function(e) {
    uri = $(e.target).attr('uri');

    $.ajax({
        type: 'GET',
        url: uri,
        dataType: 'json',
        success: function (data) {
            if (data != false) {
                // gets the nodes that should switch place
                node1 = data[0];
                node2 = data[1];

                // fin class node parent

                $higher_prio_node = $('#priority_up_' + node1).parents('.node');
                $lower_prio_node = $('#priority_up_' + node2).parents('.node');
                switch_places($higher_prio_node, $lower_prio_node);
            }
        }
    });
}

switch_places = function($higher_prio_node, $lower_prio_node) {
    $h_parent = $higher_prio_node.parent();
    $l_parent = $lower_prio_node.parent();

    $h_clone = $higher_prio_node.clone();
    $l_clone = $lower_prio_node.clone();
    $h_clone.attr('id', 'h_clone');
    $l_clone.attr('id', 'l_clone');
    
  
    // need to grab the top pos before inserting anything as some browers sums
    // the divs above height regardless of their visual height.
    $h_parent.prepend($h_clone);
    h_top = $h_clone.position().top;


    $l_parent.prepend($l_clone);
    l_top = $l_clone.position().top;
    $lower_prio_node.css('visibility', 'hidden');
    $higher_prio_node.css('visibility', 'hidden');

    $h_clone.css('position', 'absolute')
    .css('width', $higher_prio_node.css('width'))
    .css('top', h_top)
    .css('background-color', 'white');
    $l_clone.css('position', 'absolute')
    .css('width', $lower_prio_node.css('width'))
    .css('top', l_top)
    .css('background-color', 'white');




    $l_clone.animate(
        {top: $h_clone.position().top},
        500,
        function() {
            $lower_prio_node.css('visibility', 'visible');
            $h_parent.html('');
            $h_parent.append($lower_prio_node);
            $l_clone.remove();
        });


    $h_clone.animate(
        {top: $l_clone.position().top},
        500,
        function() {
            $higher_prio_node.css('visibility', 'visible');
            $l_parent.html('');
            $l_parent.append($higher_prio_node);
            $h_clone.remove();
        });

}